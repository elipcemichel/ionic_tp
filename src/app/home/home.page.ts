import { Component } from '@angular/core';
import {AuthGuardService} from '../services/auth-guard.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public username = '';

  constructor(public auth: AuthGuardService, public navCtrl: NavController) {}

  public onLogin() {
    this.auth.login(this.username);
    console.log(this.auth.canActivate());
    if (this.auth.canActivate() === true) {
       this.navCtrl.navigateRoot('list');
    } else {
       this.navCtrl.navigateRoot('home');
    }
  }

}
