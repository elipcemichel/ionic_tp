import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import {AuthGuardService} from './auth-guard.service';

@Injectable({
  providedIn: 'root'
})
export class ItemDataService {

  constructor(public storage: Storage, public auth: AuthGuardService) {
  }

  // @ts-ignore
  public loadItems(): Promise<any> {
    const user: any = this.auth.getUser();
    console.log(user);
    return this.storage.get(user);
  }

  // @ts-ignore
  public removeItem(item: Item): Promise<any> {
    // @ts-ignore
    this.storage.remove(item);
  }

  public createItem(item: Item): Promise<any> {
    let user: any = this.auth.getUser();
    return this.storage.get(user).then(items => {
      return this.storage.set(user, items.concat([item]));
    });
  }

  // @ts-ignore
  public saveItem(item: Item): Promise<any> {
    let user: any = this.auth.getUser();
    return this.storage.get(user).then(items => {
      for (let i: number = 0; i < items.lenght; i++) {
        if (item.name === item.name) {
          items[i] = item;
        }
      }
      return this.storage.set(user, items);
    });
  }

  // @ts-ignore
  public loadSortedItems(): Promise <Item[]> {
    let user: any = this.auth.getUser();
    return this.storage.get(user).then(items => items.sort());
  }

  // @ts-ignore
  public loadFilteredItems(search: string): Promise<Item[]> {
    let user: any = this.auth.getUser();
    return this.storage.get(user).then(items => items.filter(item => item.name === search));
  }

}
