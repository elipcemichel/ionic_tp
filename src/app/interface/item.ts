interface Item {
    name: string;
    description: string;
    rating: number;
}
