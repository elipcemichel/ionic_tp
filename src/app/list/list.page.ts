import { Component, OnInit } from '@angular/core';
import {ItemDataService} from '../services/item-data.service';
import {Storage} from '@ionic/storage';
import {AuthGuardService} from '../services/auth-guard.service';
import {createLoggerJob} from '@angular-devkit/core/src/experimental/jobs';
import {collectExternalReferences} from '@angular/compiler';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  public items: Array<Item> = [];
  constructor(public item: ItemDataService, public storage: Storage, public auth: AuthGuardService) { }

  ngOnInit() {
    this.storage.set(this.auth.getUser(), [{name: 'item 1', description: 'blabla', rating: 12}]);
    this.item.loadItems().then(item => this.items = item);
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
